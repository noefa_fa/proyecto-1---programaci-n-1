# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


import random
import time
import os


def imprimir(juego, vivas, tamano):
    time.sleep(0.3)
    os.system('clear')
    print('Tamaño de juego: ', tamano)
    print('Células vivas: ', vivas)
    print('Células muertas: ', (tamano*tamano) - vivas)
    # de listas a cadena de caracteres
    a = ''
    for i in range(tamano):
        for j in range(tamano):
            # ' ' para espacio al imprimir
            a += juego[i][j] + ' '
        a += '\n'
    print(a)

# cuenta la cantidad de celulas vivas y muertas


def contar_celulas(juego, tamano, retornar):
    # se cuentan solo las vivas porque el total menos las vivas son las muertas
    vivas = 0
    for i in range(tamano):
        vivas += juego[i].count('x')
    imprimir(juego, vivas, tamano)
    if retornar == 1:
        return (tamano*tamano) - vivas

# evalúa si la célula se mantiene, revive o muere


def reglas(tamano, juego, vivas):
    for i in range(tamano):
        for j in range(tamano):
            if juego[i][j] == 'x':
                if 2 <= vivas[i][j] <= 3:
                    juego[i][j] = 'x'
                elif vivas[i][j] >= 4:
                    juego[i][j] = '.'
                elif vivas[i][j] <= 1:
                    juego[i][j] = '.'
            elif juego[i][j] == '.':
                if vivas[i][j] == 3:
                    juego[i][j] = 'x'

# analiza cuantos vecinos vivos tiene cada célula


def vecinos(juego, i, j, tamano):
    """
    se guarda la célula evaluada y sea la que sea se cambia por una célula
    muertas debido a que si está viva al momento de contar los vecinos
    también se contaría a sí misma lo que provoca errores para la
    siguiente matriz al final se retorna la célula a su estado original
    y se retorna la cantidad de vecinos vivos
    """
    temp = juego[i][j]
    juego[i][j] = '.'
    vivas = 0
    for k in range(i-1, i+2):
        if 0 <= k < tamano:
            for l in range(j-1, j+2):
                if 0 <= l < tamano:
                    if juego[k][l] == 'x':
                        vivas += 1
    juego[i][j] = temp
    return vivas

# analiza vecinos para evolución del juego


def analisis(juego, tamano):
    # se inicializa matriz que guarda los vecinos de cada célula
    vivas = []
    for i in range(tamano):
        vivas.append([])
        for j in range(tamano):
            vivas[i].append(vecinos(juego, i, j, tamano))
    reglas(tamano, juego, vivas)
    # retorna las celulas muertas y se envía a imprimir
    muertas = contar_celulas(juego, tamano, 1)
    # si no hay solo células muertas, sigue el juego
    if muertas < (tamano*tamano):
        analisis(juego, tamano)

# llenar matriz con celulas aleatorias


def matriz_inicial(tamano, juego):
    for i in range(tamano):
        """
        la matriz está vacía, por lo que se comienza a llenar
        agregando las celulas vivas y muertas.
        Primero se agrega el primer parámetro o primera lista (primer for)
        Luego se agregan las células a cada lista (segundo for)
        """
        juego.append([])
        for j in range(tamano):
            celula = random.randrange(2)
            if celula == 0:
                juego[i].append('x')
            else:
                juego[i].append('.')

# procedimiento inicial


def matriz():
    # tamaño aleatorio
    tamano = random.randrange(15, 30)
    # inicialización de matriz
    juego = []
    matriz_inicial(tamano, juego)
    # evalua las celulas
    contar_celulas(juego, tamano, 0)
    # analisis del juego
    analisis(juego, tamano)


# main
os.system('clear')
print('JUEGO DE LA VIDA DE CONWAY')
matriz()
